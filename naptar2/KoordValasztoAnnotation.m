//
//  KoordValasztoAnnotation.m
//  naptar2
//
//  Created by user on 4/28/14.
//  Copyright (c) 2014 Viktor. All rights reserved.
//

#import "KoordValasztoAnnotation.h"

@implementation KoordValasztoAnnotation
double _x;
double _y;
-(id)initWithCoordX:(NSNumber*)x Y:(NSNumber*)y{
    self = [super init];
    if(self){
        _x = [x doubleValue];
        _y = [y doubleValue];
    }
    return self;
}

- (NSString *)title
{
    return @"Kiválasztott hely";
}

-(NSString *)subtitle{
    return [[NSString alloc] initWithFormat:@"%.4f %.4f", _x,_y ];
}

- (CLLocationCoordinate2D)coordinate{
    return CLLocationCoordinate2DMake(_x, _y);
}

@end
