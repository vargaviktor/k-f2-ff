//
//  Esemenyek.h
//  naptar2
//
//  Created by user on 4/26/14.
//  Copyright (c) 2014 Viktor. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Esemeny.h"

@interface Esemenyek : NSObject
-(Esemeny*) ujUresEsemeny;
-(void) hozzaadVagyFelulirEsemeny:(Esemeny *) esemeny;
-(NSInteger) countOfOsszesEsemeny;
-(Esemeny*) getEsemenyAtIndex:(int) index;
-(Esemeny*) getEsemenyByAzonosito:(int) index;
-(id) initWithTeszt;
-(void) uresEsemenyAthelyezeseAzOsszesbe;
-(void) osszesEsemenyRendezeseDatumSzerint;

-(NSString*) getOsszesEsemenyJSONje;
-(NSString*) getModosultEsemenyekJSONje;
-(void) setModosultToNOInEsemenyek;
@end

