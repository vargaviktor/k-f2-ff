//
//  SzerkesztViewController.m
//  naptar2
//
//  Created by user on 4/26/14.
//  Copyright (c) 2014 Viktor. All rights reserved.
//

#import "SzerkesztViewController.h"
#import "UITableViewControllerExample.h"
#import "KoordPickerViewController.h"
#import "KoordValasztoAnnotation.h"

@interface SzerkesztViewController ()
@property (strong, nonatomic) KoordPickerViewController *vc;
@end

@implementation SzerkesztViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.    
    _Eneve.text = _SVCesemeny.neve;
    [_Ekezdete setDate:_SVCesemeny.kezdete];
    [_Evege setDate: _SVCesemeny.vege];
    
    [_riasztasIn setValue: _SVCesemeny.warnInSugar.doubleValue];
    _riasztasInLabel.text = [_SVCesemeny.warnInSugar stringValue];
    
    [_riasztasOut setValue: _SVCesemeny.warnOutSugar.doubleValue];
    _riasztasOutLabel.text = [_SVCesemeny.warnOutSugar stringValue];
    
    [_sugar setValue: _SVCesemeny.sugar.doubleValue];
    _sugarLabel.text = [_SVCesemeny.sugar stringValue];
    
    _szelesseg = _SVCesemeny.szelesseg;
    _hosszusag = _SVCesemeny.hosszusag;
    
    [[NSNotificationCenter defaultCenter] addObserverForName:@"hideKeyboard" object:nil queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification *note) {
        [self.view endEditing:YES];
    }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)eNeveValueChanged:(UITextField *)sender {
    [_SVCesemeny setNeve: sender.text];
}

- (IBAction)eVegeValueChanged:(UIDatePicker *)sender {
    [_SVCesemeny setVege: sender.date];
}

- (IBAction)eKezdeteValueChanged:(UIDatePicker *)sender {
    [_SVCesemeny setKezdete: sender.date];
}

- (IBAction)riasztasOutValueChanged:(UIStepper *)sender {
    double value = [sender value];
    
    [_riasztasOutLabel setText:[NSString stringWithFormat:@"%d", (int)value]];
    [_SVCesemeny setWarnOutSugar: [NSNumber numberWithDouble:value]];
}

- (IBAction)riasztasInValueChanged:(UIStepper *)sender {
    double value = [sender value];
    
    [_riasztasInLabel setText:[NSString stringWithFormat:@"%d", (int)value]];
    [_SVCesemeny setWarnInSugar: [NSNumber numberWithDouble:value]];
}

- (IBAction)sugarValueChanged:(UIStepper *)sender {
    double value = [sender value];
    
    [_sugarLabel setText:[NSString stringWithFormat:@"%d", (int)value]];
    [_SVCesemeny setSugar: [NSNumber numberWithDouble:value]];
}

- (IBAction)megnyitTerkep:(id)sender {
    
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    _vc = segue.destinationViewController;
    if ([segue.identifier isEqualToString:@"megnyitTerkep"]){
        _vc.SVCesemeny = _SVCesemeny;
    }
}
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"hideKeyboard" object:nil];
}
@end
