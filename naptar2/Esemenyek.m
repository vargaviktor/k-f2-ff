//
//  Esemenyek.m
//  naptar2
//
//  Created by user on 4/26/14.
//  Copyright (c) 2014 Viktor. All rights reserved.
//

#import "Esemenyek.h"

@implementation Esemenyek

NSMutableArray *_osszesEsemeny;
Esemeny *uresEsemeny;

-(void) hozzaadEsemeny:(Esemeny *) esemeny{
    [_osszesEsemeny addObject:esemeny];
}

-(void) hozzaadVagyFelulirEsemeny:(Esemeny *) esemeny{
    Esemeny *es = [self getEsemenyByAzonosito:[esemeny.azonosito intValue]];
    if (es == nil) {
        [self hozzaadEsemeny:esemeny];
    }else{
        es = esemeny;
    }
}

-(NSInteger)countOfOsszesEsemeny{
    return _osszesEsemeny.count;
}

-(Esemeny *)getEsemenyAtIndex:(int)index{
    return _osszesEsemeny[index];
}

-(Esemeny *)getEsemenyByAzonosito:(int)index{
    return [self keresAdottAzonositojut:index];
}

-(id)init{
    self = [super init];
    if(self)
        _osszesEsemeny = [[NSMutableArray alloc] init];
    return self;
}

-(id)initWithTeszt{
    self = [self init];
    if (self){
        for (int i=0; i < 5; i++) {
            Esemeny *es = [[Esemeny alloc] init];
            [es setAzonosito:[NSNumber numberWithInt: i]];
            es.neve = [NSString stringWithFormat:@"Test %d" ,i];
            es.sugar = [NSNumber numberWithInt: 53 + i ];
            es.warnInSugar = [NSNumber numberWithInt: 24 + i ];
            es.warnOutSugar = [NSNumber numberWithInt: 12 + i ];
//            es.kezdete = [NSDate initWithTimeIntervalSinceNow: 200];

            [self hozzaadEsemeny:es];
        }
    }
    return self;
}

-(Esemeny*) ujUresEsemeny{
    if(uresEsemeny == nil)
        uresEsemeny = [[Esemeny alloc]init];
    return uresEsemeny;
}

-(void) uresEsemenyAthelyezeseAzOsszesbe{
    if(uresEsemeny == nil) return;
    [uresEsemeny setAzonosito:[self kovetkezoAzonosito]];
    
    [self hozzaadEsemeny:uresEsemeny];
    
    uresEsemeny = nil;
}

-(NSNumber*) kovetkezoAzonosito{
    int max = 0;
    for (Esemeny *es in _osszesEsemeny) {
        if (max < [es.azonosito intValue]) max = [es.azonosito intValue];
    }
    return [NSNumber numberWithInt:max + 1];
}

-(Esemeny*) keresAdottAzonositojut:(int) azonosito{
    for (Esemeny *es in _osszesEsemeny) {
        if([es.azonosito intValue] == azonosito) return es;
    }
    return nil;
}
-(void) osszesEsemenyRendezeseDatumSzerint{
    [_osszesEsemeny sortUsingComparator:^NSComparisonResult(Esemeny* obj1, Esemeny* obj2) {
        return [[obj2 kezdete] compare:[obj1 kezdete]];
    }];
}

-(NSString *)getOsszesEsemenyJSONje{
    return [self getJSONCsakAModosultakat:NO];
}
-(NSString*) getModosultEsemenyekJSONje{
    return [self getJSONCsakAModosultakat:YES];
}
-(NSString*) getJSONCsakAModosultakat:(BOOL) csakAModosultak{
    NSMutableArray *osszesDictionaryFromEsemenyek = [[NSMutableArray alloc] init];
    for (Esemeny *esemeny in _osszesEsemeny) {
        if (csakAModosultak)
            if (!esemeny.modosultE) continue;
        NSDictionary *egyDictionaryFromEsemeny = [NSDictionary dictionaryWithObjectsAndKeys:
                                                  esemeny.neve,      @"neve",
                                                  esemeny.azonosito, @"azonosito",
                                                  [NSNumber numberWithDouble:[esemeny.kezdete timeIntervalSince1970]],   @"kezdete",
                                                  [NSNumber numberWithDouble:[esemeny.vege timeIntervalSince1970]],      @"vege",
                                                  esemeny.warnInSugar, @"warnInSugar",
                                                  esemeny.warnOutSugar, @"warnOutSugar",
                                                  esemeny.sugar,     @"sugar",
                                                  esemeny.szelesseg, @"szelesseg",
                                                  esemeny.hosszusag, @"hosszusag",
                                                  nil];
        [osszesDictionaryFromEsemenyek addObject:egyDictionaryFromEsemeny];
    }
    NSData *jsonData2 = [NSJSONSerialization dataWithJSONObject:osszesDictionaryFromEsemenyek options:NSJSONWritingPrettyPrinted error:nil];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData2 encoding:NSUTF8StringEncoding];
    return jsonString;
}

-(void) setModosultToNOInEsemenyek{
    for (Esemeny *esemeny in _osszesEsemeny) {
        esemeny.modosultE = NO;
    }
}
@end


















