//
//  Esemeny.m
//  naptar2
//
//  Created by user on 4/26/14.
//  Copyright (c) 2014 Viktor. All rights reserved.
//

#import "Esemeny.h"

@implementation Esemeny
-(id)init{
    self = [super init];
    if(self){
        _azonosito = [NSNumber numberWithInt:-1];
        _kezdete = [[NSDate alloc] init];
        _vege = [[NSDate alloc] init];
        _warnInSugar = [NSNumber numberWithInt:10];
        _warnOutSugar = [NSNumber numberWithInt:15];
        _sugar = [NSNumber numberWithInt:0];
        _szelesseg = [NSNumber numberWithInt:0];
        _hosszusag = [NSNumber numberWithInt:0];
        
        _modosultE = YES;
    }
    return self;
}

-(void)setAzonosito:(NSNumber *)azonosito{
    _modosultE = YES;
    _azonosito = azonosito;
}
-(void)setHosszusag:(NSNumber *)hosszusag{
    _modosultE = YES;
    _hosszusag = hosszusag;
}
-(void)setKezdete:(NSDate *)kezdete{
    _modosultE = YES;
    _kezdete = kezdete;
}
-(void)setNeve:(NSString *)neve{
    _modosultE = YES;
    _neve = neve;
}
-(void)setSugar:(NSNumber *)sugar{
    _modosultE = YES;
    _sugar = sugar;
}
-(void)setSzelesseg:(NSNumber *)szelesseg{
    _modosultE = YES;
    _szelesseg = szelesseg;
}
-(void)setVege:(NSDate *)vege{
    _modosultE = YES;
    _vege = vege;
}
-(void)setWarnInSugar:(NSNumber *)warnInSugar{
    _modosultE = YES;
    _warnInSugar = warnInSugar;
}
-(void)setWarnOutSugar:(NSNumber *)warnOutSugar{
    _modosultE = YES;
    _warnOutSugar = warnOutSugar;
}
-(void)setValuesForKeysWithDictionary:(NSDictionary *)keyedValues{
    NSMutableDictionary *dic = [keyedValues mutableCopy];
    NSNumber *kezd = [dic valueForKey:@"kezdete"];
    if(kezd)
       _kezdete = [_kezdete initWithTimeIntervalSince1970:[kezd doubleValue]];
    
    NSNumber *vege = [dic valueForKey:@"vege"];
    if(vege)
        _vege = [_vege initWithTimeIntervalSince1970:[vege doubleValue]];
    
    [dic removeObjectsForKeys:@[@"kezdete",@"vege"]];
    [super setValuesForKeysWithDictionary:dic];
}

-(NSString*) getDetail{
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setDateFormat:@"MM. dd. HH:mm"];
    NSDateFormatter *formatv = [[NSDateFormatter alloc] init];
    [formatv setDateFormat:@"HH:mm"];
    
    unsigned int flags = NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit;
    NSCalendar* calendar = [NSCalendar currentCalendar];
    NSDateComponents* components = [calendar components:flags fromDate:_kezdete];
    NSDate* csakDatumKezdet = [calendar dateFromComponents:components];
    
    NSDateComponents* componentsV = [calendar components:flags fromDate:_vege];
    NSDate* csakDatumVege = [calendar dateFromComponents:componentsV];
    
    if(![csakDatumKezdet isEqualToDate:csakDatumVege])
        formatv = format;
    
    return [NSString stringWithFormat:@"%@ - %@", [format stringFromDate:_kezdete], [formatv stringFromDate:_vege]];
}
-(NSString*) getTitle{
    return [NSString stringWithFormat:@"[%@] %@", _azonosito, _neve];
}

// zöld: kész (vége < ma)   (2)most  (1)vege
// piros: futás alatt
// narancs: majd (kezdet > ma)  (1)most   (2)kezdet
-(UIColor*) getColor{
    // compare: (1)this .. (2)paraméter ASC v DESC
    NSDate *most = [[NSDate alloc] init];
    if ([most compare:_vege] == NSOrderedDescending){
        // zöld
        return [UIColor greenColor];
    }else if([most compare:_kezdete] == NSOrderedAscending){
        // narancs
        return [UIColor orangeColor];
    }else{
        // piros
        return [UIColor redColor];
    }
}

@end
