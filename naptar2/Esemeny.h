//
//  ;
//  naptar2
//
//  Created by user on 4/26/14.
//  Copyright (c) 2014 Viktor. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Esemeny : NSObject
@property (strong, nonatomic) NSNumber *azonosito;
@property (strong, nonatomic) NSString *neve;
@property (strong, nonatomic) NSDate *kezdete;
@property (strong, nonatomic) NSDate *vege;
@property (strong, nonatomic) NSNumber *warnInSugar;
@property (strong, nonatomic) NSNumber *warnOutSugar;
@property (strong, nonatomic) NSNumber *szelesseg;
@property (strong, nonatomic) NSNumber *hosszusag;
@property (strong, nonatomic) NSNumber *sugar;

@property (assign, nonatomic) BOOL modosultE;

-(id)init;
-(NSString*) getDetail;
-(NSString*) getTitle;
-(UIColor*) getColor;
@end
