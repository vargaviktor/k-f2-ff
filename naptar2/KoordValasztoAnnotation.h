//
//  KoordValasztoAnnotation.h
//  naptar2
//
//  Created by user on 4/28/14.
//  Copyright (c) 2014 Viktor. All rights reserved.
//
#import <MapKit/MapKit.h>
#import <UIKit/UIKit.h>

@interface KoordValasztoAnnotation : NSObject<MKAnnotation>
-(id)initWithCoordX:(NSNumber*)x Y:(NSNumber*)y;
@end
