//
//  KoordPickerViewController.m
//  naptar2
//
//  Created by user on 4/28/14.
//  Copyright (c) 2014 Viktor. All rights reserved.
//
#import <MapKit/MapKit.h>
#import "KoordPickerViewController.h"
#import "KoordValasztoAnnotation.h"

@interface KoordPickerViewController ()
@property (weak, nonatomic) IBOutlet MKMapView *terkepView;
@property (strong, nonatomic) KoordValasztoAnnotation *dropPin;
@end

@implementation KoordPickerViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    UILongPressGestureRecognizer *longPressGesture = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongPressGesture:)];
    [self.terkepView addGestureRecognizer:longPressGesture];
    
    CLLocationCoordinate2D coord = {.latitude =  [_SVCesemeny.szelesseg doubleValue], .longitude = [_SVCesemeny.hosszusag doubleValue]};
    MKCircle *circle = [MKCircle circleWithCenterCoordinate:coord radius:100];
    [_terkepView addOverlay:circle];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if ([_SVCesemeny.szelesseg doubleValue] != 0 || [_SVCesemeny.hosszusag doubleValue] != 0){
        CLLocationCoordinate2D coord = {.latitude =  [_SVCesemeny.szelesseg doubleValue], .longitude = [_SVCesemeny.hosszusag doubleValue]};
        MKCoordinateSpan span = {.latitudeDelta =  1, .longitudeDelta =  1};
        MKCoordinateRegion region = {coord, span};
        [_terkepView setRegion:region];
        [self dropPinToMap];
    }
}

-(void)handleLongPressGesture:(UIGestureRecognizer*)sender {    // This is important if you only want to receive one tap and hold event
    if (sender.state != UIGestureRecognizerStateEnded){
        // Here we get the CGPoint for the touch and convert it to latitude and longitude coordinates to display on the map
        CGPoint point = [sender locationInView:self.terkepView];
        CLLocationCoordinate2D locCoord = [self.terkepView convertPoint:point toCoordinateFromView:self.terkepView];
        [_SVCesemeny setSzelesseg: [[NSNumber alloc] initWithDouble:locCoord.latitude]];
        [_SVCesemeny setHosszusag: [[NSNumber alloc] initWithDouble:locCoord.longitude]];
        [self dropPinToMap];
        
        // Then all you have to do is create the annotation and add it to the map
        
    }
}

- (void)dropPinToMap{
    _dropPin = [[KoordValasztoAnnotation alloc] initWithCoordX:_SVCesemeny.szelesseg Y:_SVCesemeny.hosszusag];
    [self.terkepView addAnnotation:_dropPin];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
