//
//  SzerkesztViewController.h
//  naptar2
//
//  Created by user on 4/26/14.
//  Copyright (c) 2014 Viktor. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Esemeny.h"

@interface SzerkesztViewController : UIViewController

@property (strong, nonatomic) Esemeny *SVCesemeny;

@property (weak, nonatomic) IBOutlet UITextField *Eneve;
@property (weak, nonatomic) IBOutlet UIDatePicker *Evege;
@property (weak, nonatomic) IBOutlet UIDatePicker *Ekezdete;

@property (weak, nonatomic) IBOutlet UIStepper *riasztasOut;
@property (weak, nonatomic) IBOutlet UILabel *riasztasOutLabel;

@property (weak, nonatomic) IBOutlet UIStepper *riasztasIn;
@property (weak, nonatomic) IBOutlet UILabel *riasztasInLabel;

@property (weak, nonatomic) IBOutlet UIStepper *sugar;
@property (weak, nonatomic) IBOutlet UILabel *sugarLabel;

@property (strong, nonatomic) NSNumber *szelesseg;
@property (strong, nonatomic) NSNumber *hosszusag;

- (IBAction)eNeveValueChanged:(id)sender;
- (IBAction)eVegeValueChanged:(id)sender;
- (IBAction)eKezdeteValueChanged:(id)sender;
- (IBAction)riasztasOutValueChanged:(id)sender;
- (IBAction)riasztasInValueChanged:(id)sender;
- (IBAction)sugarValueChanged:(id)sender;
- (IBAction)megnyitTerkep:(id)sender;
@end
