//
//  SzerkesztUIScrollView.m
//  naptar2
//
//  Created by user on 5/3/14.
//  Copyright (c) 2014 Viktor. All rights reserved.
//

#import "SzerkesztUIScrollView.h"

@implementation SzerkesztUIScrollView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"hideKeyboard" object:nil];
    [super touchesBegan:touches withEvent:event];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
