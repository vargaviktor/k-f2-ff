//
//  halozatiKommunikacio.h
//  naptar2
//
//  Created by user on 4/29/14.
//  Copyright (c) 2014 Viktor. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFNetworking.h"
#import "Esemenyek.h"

@interface halozatiKommunikacio : NSObject
@property (strong, nonatomic) NSURL *domainName;
@property (strong, nonatomic) Esemenyek *esemenyekInstance;

-(void) getEsemenyekFromServerToEsemenyekInstance;
-(void) sendEsemenyekToServer;
@end
