//
//  halozatiKommunikacio.m
//  naptar2
//
//  Created by user on 4/29/14.
//  Copyright (c) 2014 Viktor. All rights reserved.
//

#import "halozatiKommunikacio.h"

@implementation halozatiKommunikacio
AFHTTPRequestOperationManager *manager;

-(void) initalize{
    _domainName = [NSURL URLWithString:@"http://users.atw.hu/ffsuli3"];
    if(manager == nil){
        manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:_domainName];
        manager.requestSerializer = [AFJSONRequestSerializer serializer];
    }
}

-(void) getEsemenyekFromServerToEsemenyekInstance{
    [self initalize];
    [manager GET:@"getJSON.php" parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self getEsemenyekFromJSON:responseObject];
        [_esemenyekInstance setModosultToNOInEsemenyek];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"getEsemenyekFromServerReturn" object:@"kesz"];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"%@", error);
    }];
}

-(void) getEsemenyekFromJSON:(NSArray*) json{
    for(NSDictionary *item in json) {
        Esemeny *es = [[Esemeny alloc] init];
        [es setValuesForKeysWithDictionary:item];
        [_esemenyekInstance hozzaadVagyFelulirEsemeny:es];
    }
}



-(void) sendEsemenyekToServer{
    [self initalize];
    [manager POST:@"getJSON.php" parameters:@{@"esemenyekJSON": [_esemenyekInstance getOsszesEsemenyJSONje]} success:^(AFHTTPRequestOperation *operation, id responseObject) {
        // ráadásul amúgy, ha mondjuk olyan oldalt hívok meg, amely nem application/json Content-Type-al tér vissza, akkor elszáll error-al
        // ezt le tudod tesztelni ha meghívod az index.php-t
        // ez is furcsa, de jelenleg most nem zavar, mert megoldtam (azért a getJSON.php-t hívom meg)
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"sendEsToServ %@", error);
    }];
}
@end